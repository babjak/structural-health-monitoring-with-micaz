#include "Events.h"
#include "Events_Descriptor.h"
#define ARRAYSIZE(x) (sizeof(x)/sizeof(*x))

#define AE_TIMER_PERIOD_MILLI  10


generic module AE_analyserP()
{
    uses       interface Boot;

    uses       interface Timer<TMilli>    as Timer;

	provides   interface Read<Event_t>    as output;
}
implementation
{
    volatile uint32_t time_ms_counter        = 0;
    volatile uint32_t prev_time_ms_counter   = 0;

    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    task void Read_Task()
    {
        error_t     result;
        uint16_t    val;
        uint16_t    i;

        result = SUCCESS;
        val = 0;

        if (time_ms_counter == prev_time_ms_counter)
            goto end_label;

        prev_time_ms_counter = time_ms_counter;

        for ( i = 0; i < ARRAYSIZE(events); i++ )
            if ( time_ms_counter - AE_TIMER_PERIOD_MILLI < events[i].frame_time_ms 
                && 
                time_ms_counter >= events[i].frame_time_ms )
            {
//                printStr("time_ms_counter == events[i].frame_time_ms");
                printStr("########################################################");
                printStr("New event detected");
                printStr("time [ms]");
                printInt32(time_ms_counter);
//                printStr("events[i].frame_time_ms");
//                printInt32(events[i].frame_time_ms);

                signal output.readDone(result, events[i]);
            }

end_label:
        post Read_Task();
    }

    event void Boot.booted()
    {
//        printStr("AE_analyserP Boot.booted()");

        call Timer.startPeriodic( AE_TIMER_PERIOD_MILLI );
        post Read_Task();
    }

    event void Timer.fired()
    {
        time_ms_counter += AE_TIMER_PERIOD_MILLI;
    }  

    command error_t output.read()
    {
        return SUCCESS;
    }
}
