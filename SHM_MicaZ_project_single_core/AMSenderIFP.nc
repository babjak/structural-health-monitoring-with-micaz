#include "RadioMsg.h"

#ifndef MAX 
#define MAX(a,b) ((a) > (b) ? (a) : (b)) 
#endif

#ifndef MIN 
#define MIN(a,b) ((a) < (b) ? (a) : (b)) 
#endif 

module AMSenderIFP 
{
	provides interface RFMsgSend;

    //Radio TX
    //AMSenderC
    uses interface Packet;
    //uses interface AMPacket;
    uses interface AMSend;
}
implementation
{
    bool        busy   = FALSE;
    message_t   pkt;
  
    command	void RFMsgSend.SendRadioMsg( uint16_t nodeid, void* buffer, uint8_t buffer_size )
    {
		RadioMsg* rpkt;
        error_t     err;

//        printStr("buffer_size");
//        printInt32((int32_t) buffer_size );
//        printStr("sizeof (RadioMsg)");
//        printInt32((int32_t) sizeof (RadioMsg) );

        if (busy) 
            return;
            
        rpkt = (RadioMsg*) ( call Packet.getPayload(&pkt, sizeof (RadioMsg)) );
        rpkt->nodeid    = nodeid;
        memcpy( rpkt->buffer, buffer, MIN(RADIO_MSG_BUFFER_SIZE, buffer_size) * sizeof(uint8_t) );
        
        err = call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(RadioMsg));

//        printStr("err");
//        printInt32((int32_t) err);

        if ( err == SUCCESS )
        { 
            busy = TRUE;
//            printStr("So far so good.");
        }
    }

    event void AMSend.sendDone(message_t* msg, error_t error)
    {
        if (&pkt == msg) 
            busy = FALSE;
    }    
   
}
