#include <math.h>
#include <stdlib.h> 
#include <string.h> 
#include "Events_Descriptor.h"
#include "Classification_Descriptor.h"


#define DATA_NUM            10
//#define DATA_DIMENSION      NUMBER_OF_EVENT_CHANNELS

#define XI                  0.05 // 0.01 < x < 1
#define STEEP_TOLERANCE_PTS 0 
#define MIN_CLUSTER_PTS     4

#define EPSILON             1000.0
#define MIN_PTS             3


#ifndef MAX 
#define MAX(a,b) ((a) > (b) ? (a) : (b)) 
#endif

#ifndef MIN 
#define MIN(a,b) ((a) < (b) ? (a) : (b)) 
#endif 

//#ifndef BEN_ASSERT 
//#define BEN_ASSERT(a) (if(!a){printStr(#a);}) 
//#define BEN_ASSERT(a) (printStr(a ? "" ! #a)) 
//#endif 



typedef int16_t data_t;
#define DATA_MAX            INT16_MAX


// For ordered seeds and neighbors
typedef struct point_s
{
    point_cnt_t     idx;  
    float           dist;
} point_t;

typedef struct point_list_s
{
    point_cnt_t     len;
    point_t         list[DATA_NUM];
} point_list_t;

typedef struct range_s
{
    point_cnt_t     start;  
    point_cnt_t     stop;
} range_t;

typedef struct range_list_s
{
    point_cnt_t     len;  
    range_t*        list;
} range_list_t;


generic module Optics_ClusteringP()
{
	provides interface Read<classification_msg_t> as output;

    uses interface Read<Event_t> as input;
}
implementation
{
    data_t          data[DATA_NUM][DATA_DIMENSION] =  
    {
        { -4500, -500 },   // noise
//        { -4500, 1000 },   // noise
//        { -4500,  500 },   // noise
        { -5000,  500 },   // noise
        { -3000,  230 },   // valid 
        { -3100,  250 },   // valid
        { -3000,  270 },   // valid
        { -2900,  250 },   // valid
        { -3500,  880 },   // valid
        { -3600,  900 },   // valid 
        { -3500,  920 },   // valid
        { -3400,  900 }   // valid
    };

    point_cnt_t     N = DATA_NUM;

    volatile bool            processed[DATA_NUM]; 
    volatile point_cnt_t     data_idxs[DATA_NUM]; 
    volatile float           reach_dists[DATA_NUM];

    volatile data_t          new_data[DATA_DIMENSION];


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    bool Is_Point_List_Empty(point_list_t*    p_points)
    {
        return p_points->len ? FALSE : TRUE;
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void Clear_Point_List(point_list_t*    p_points)
    {
        p_points->len = 0;
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    float Get_Distance( point_cnt_t idx1, 
                        point_cnt_t idx2 )
    {
        float   result = 0;
        uint8_t i;

//        printStr("Get_Distance");

//        printStr("idx1");
//        printInt32((int32_t) idx1);
//        printStr("idx2");
//        printInt32((int32_t) idx2);

        for ( i = 0; i < DATA_DIMENSION; i++ )
        {
//            printStr("data[idx1][i]");
//            printInt32((int32_t) data[idx1][i]);
//            printStr("data[idx2][i]");
//            printInt32((int32_t) data[idx2][i]);

            result += pow(((float) data[idx1][i]) - ((float) data[idx2][i]), 2);
//            printStr("Result");
//            printInt32((int32_t) result);
        }

//        printStr("Result");
//        printInt32((int32_t) result);
//        printStr("sqrt(result)");
//        printInt32((int32_t) sqrt(result));

        return sqrt(result);
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    int Compare_Points (const void* a, const void* b)
    {
        if ( ((point_t*)a)->dist <  ((point_t*)b)->dist ) 
            return -1;

        if ( ((point_t*)a)->dist >  ((point_t*)b)->dist ) 
            return 1;

        if ( ((point_t*)a)->idx <  ((point_t*)b)->idx ) 
            return -1;

        if ( ((point_t*)a)->idx >  ((point_t*)b)->idx ) 
            return 1;

        return 0;
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void Get_Neighbors( point_list_t*   p_neighbors,
                        point_cnt_t     idx )
    {
        float           dist;
        point_cnt_t     i;

//        printStr("Get_Neighbors");

        for ( i = 0; i < N; i++ )
        {
            if ( idx == i )
                continue;

            dist = Get_Distance(idx, i);

//            printStr("Get_Neighbors dist");
//            printInt32((int32_t) dist);        

            if ( dist > EPSILON )
                continue;

//            if (dist == 0)
//            {
//                printStr("Get_Neighbors dist");
//                printInt32((int32_t) dist);        
//            }

            p_neighbors->list[p_neighbors->len].idx = i;
            p_neighbors->list[p_neighbors->len].dist = dist;
            p_neighbors->len++;

//            printStr("p_neighbors->len");
//            printInt32((int32_t) p_neighbors->len);        

        }

//        printStr("neighbors.len");
//        printInt32((int32_t) neighbors.len);

        qsort(p_neighbors->list, 
            p_neighbors->len, 
            sizeof(point_t), 
            Compare_Points ); 
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    bool Is_Core_Object_cnt( point_cnt_t neighbor_cnt )
    {
//        printStr("Is_Core_Object_cnt");

        return (neighbor_cnt >= MIN_PTS) ? TRUE : FALSE;
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    bool Is_Core_Object( point_cnt_t idx )
    {
        point_list_t    neighbors;

        neighbors.len = 0;

//        printStr("Is_Core_Object");

        Get_Neighbors(&neighbors, idx);

        return Is_Core_Object_cnt(neighbors.len);
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    float Get_Core_Distance_neighbors( point_list_t*    p_neighbors )
    {
//       printStr("Get_Core_Distance_neighbors");

        if ( !Is_Core_Object_cnt(p_neighbors->len) ) // Not a core object 
            return INFINITY;

        return p_neighbors->list[ MIN_PTS-1 ].dist;
    }



    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    float Get_Core_Distance( point_cnt_t idx )
    {
        point_list_t    neighbors;
        float           dist;

//        printStr("Get_Core_Distance");

        neighbors.len = 0;

        Get_Neighbors( &neighbors, idx );

        return Get_Core_Distance_neighbors( &neighbors );
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void Update_Ordered_Seeds(  point_cnt_t     idx_in, 
                                point_list_t*   p_ordered_seeds )
    {
        point_list_t    neighbors;
        float           core_distance;
        point_cnt_t     i;

        neighbors.len = 0;

//        printStr("Update_Ordered_Seeds");

        Get_Neighbors( &neighbors, idx_in );
        core_distance   = Get_Core_Distance_neighbors( &neighbors );

//        printStr("U idx_in");
//        printInt32((int32_t) idx_in);
//        printStr("U neighbors.len");
//        printInt32((int32_t) neighbors.len);
//        printStr("U core_distance");
//        printInt32((int32_t) core_distance);

        for ( i = 0; i < neighbors.len; i++ )
        {
            point_cnt_t     idx;
            float           dist;
            float           new_reach_dist;
            point_cnt_t     j;

            idx = neighbors.list[ i ].idx;
            dist = neighbors.list[ i ].dist;

//            printStr("U i");
//            printInt32((int32_t) i);

//            printStr("U idx");
//            printInt32((int32_t) idx);

//            printStr("U dist");
//            printInt32((int32_t) dist);

//            printStr("U proc");
//            if (processed[ idx ])
//                printStr("True");
//            else
//                printStr("False");

            // If point has already been processed don't bother 
            if ( processed[ idx ] )
                continue;

            new_reach_dist = MAX( core_distance, dist );

//            printStr("new_reach_dist");
//            printInt32((int32_t) new_reach_dist);
//            printStr("p_ordered_seeds->len");
//            printInt32((int32_t) p_ordered_seeds->len);

            for ( j = 0; j <= p_ordered_seeds->len; j++ )
            {   
//                printStr("/tUOS for");

                if ( j >= p_ordered_seeds->len )
                {
                    p_ordered_seeds->list[p_ordered_seeds->len].idx = idx;
                    p_ordered_seeds->list[p_ordered_seeds->len].dist = new_reach_dist;
                    p_ordered_seeds->len++;
                    break;
                }

                if ( p_ordered_seeds->list[ j ].idx == idx )
                {
                    p_ordered_seeds->list[ j ].dist = MIN( p_ordered_seeds->list[ j ].dist, new_reach_dist );
                    break;
                }
            }
        }

//        if ( !Is_Point_List_Empty( &neighbors ) )
//            Clear_Point_List( &neighbors );

        qsort(p_ordered_seeds->list, 
            p_ordered_seeds->len, 
            sizeof(point_t), 
            Compare_Points ); 
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void Generate_Reach_Dist( )
    {
        point_cnt_t    i;
        point_cnt_t    reach_idx = 0;
        point_list_t   ordered_seeds;

//        printStr("Generate_Reach_Dist()");

        ordered_seeds.len = 0;

//        memset( processed, FALSE, N );
        for ( i = 0; i < N; i++ )
            processed[ i ] = FALSE;

        for ( i = 0; i < N; i++ )
        {
//            printStr("for ( i = 0; i < N; i++ )");
//            printStr("N = ");
//            printInt8(N);
//            printStr("i = ");
//            printInt32((uint32_t) i);

            if ( processed[ i ] )
                continue;

            ordered_seeds.list[ 0 ].idx = i;
            ordered_seeds.list[ 0 ].dist = INFINITY;
            ordered_seeds.len = 1;

//            printStr("ordered_seeds.len = ");
//            printInt8(ordered_seeds.len); 

            while ( !Is_Point_List_Empty( &ordered_seeds ) )
            {
                point_cnt_t     idx;
                float           reach_dist;

//                for ( j = 0; j < ordered_seeds.len; j++ ) 
//                {
//                    printStr("ordered_seeds.list[ j ].idx");
//                    printInt32( (int32_t) ordered_seeds.list[ j ].idx );
//                    printStr("ordered_seeds.list[ j ].dist");
//                    printInt32( (int32_t) ordered_seeds.list[ j ].dist );
//                }

//                printStr("ordered_seeds.len = ");
//                printInt8(ordered_seeds.len);                

                idx         = ordered_seeds.list[ 0 ].idx;
                reach_dist  = ordered_seeds.list[ 0 ].dist;

//                printStr("ordered_seeds.len = ");
//                printInt32( (int32_t) ordered_seeds.len); 

//                printStr("Reach_dist idx");
//                printInt32( (int32_t) idx);  

//                printStr("reach_dist");
//                printInt32((int32_t) reach_dist);

                ordered_seeds.len -= 1;

                if ( ordered_seeds.len )
                {
                    memmove( ordered_seeds.list, 
                            ordered_seeds.list + 1, 
                            ordered_seeds.len * sizeof(point_t) );

//                    printStr("ordered_seeds.list");
//                    printInt32( (int32_t) (ordered_seeds.list));
//                    printStr("ordered_seeds.len * sizeof(point_list_t)");
//                    printInt32( (int32_t) (ordered_seeds.len * sizeof(point_list_t)));
                }
//                else
//                    Clear_Point_List( &ordered_seeds );

                processed[ idx ]    = TRUE;

                if ( reach_idx < N )
                {
                    reach_dists[ reach_idx ]    = reach_dist;
                    data_idxs[ reach_idx ]      = idx;

                    reach_idx++;
                }
                else
                    printStr("ERROR Reach dists out of bound");

                if ( !Is_Core_Object( idx ) )
                {
//                    printStr("Not core object...");
                    continue;
                }

//                printStr("Core object");

//                printStr("ordered_seeds before");

//                printStr("idxs");
//                for ( j = 0; j < ordered_seeds.len; j++ )
//                    printInt32( (int32_t) ordered_seeds.list[j].idx );

//                printStr("dists");
//                for ( j = 0; j < ordered_seeds.len; j++ )
//                    printInt32( (int32_t) ordered_seeds.list[j].dist );


                Update_Ordered_Seeds( idx, &ordered_seeds );


//                printStr("ordered_seeds after");

//                printStr("idxs");
//                for ( j = 0; j < ordered_seeds.len; j++ )
//                    printInt32( (int32_t) ordered_seeds.list[j].idx );

//                printStr("dists");
//                for ( j = 0; j < ordered_seeds.len; j++ )
//                    printInt32( (int32_t) ordered_seeds.list[j].dist );

            }

//            printStr("ordered_seeds.list");
//            printInt8(ordered_seeds.len); 

            if ( !Is_Point_List_Empty( &ordered_seeds ) )
                Clear_Point_List( &ordered_seeds );
        }
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    range_list_t Append_Ranges( range_list_t    *p_ranges,
                                point_cnt_t     start, 
                                point_cnt_t     stop )
    {
        //TODO, make sure ranges.len does not overflow
//        printStr("\t -> realloc Append_Ranges");
        p_ranges->list = (range_t*) realloc( p_ranges->list, (p_ranges->len+1) * sizeof(range_t) );

        p_ranges->list[p_ranges->len].start   = start;
        p_ranges->list[p_ranges->len].stop    = stop;

        p_ranges->len += 1;
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    int Compare_Ranges (const void* a, const void* b)
    {
        if ( ((range_t*)a)->stop - ((range_t*)a)->start < ((range_t*)b)->stop - ((range_t*)b)->start ) 
            return -1;

        if ( ((range_t*)a)->stop - ((range_t*)a)->start > ((range_t*)b)->stop - ((range_t*)b)->start ) 
            return 1;

        return 0;
    }


/*
    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void Add_To_Ordered_Ranges( range_list_t*   p_ranges,
                                point_cnt_t     start, 
                                point_cnt_t     stop )
    {
        //TODO, make sure points.len does not overflow
        //TODO, make sure realloc worked

        point_cnt_t    bigger_range_idx;

        for ( bigger_range_idx = 0; bigger_range_idx < p_ranges->len; bigger_range_idx++ )
            if ( p_ranges->list[ bigger_range_idx ].stop - p_ranges->list[ bigger_range_idx ].start > stop - start )
                break;

//        printStr("\t -> realloc Add_To_Ordered_Ranges");
        p_ranges->list = (range_t*) realloc( p_ranges->list, (p_ranges->len+1) * sizeof(range_t) );

        memmove( p_ranges->list + bigger_range_idx + 1, 
                p_ranges->list + bigger_range_idx, 
                (p_ranges->len - bigger_range_idx) * sizeof(range_t) );

        p_ranges->list[ bigger_range_idx ].start  = start;
        p_ranges->list[ bigger_range_idx ].stop   = stop;

        p_ranges->len += 1;
    }
*/

    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void Look_For_Clusters( range_t         U, 
                            range_list_t*   p_steep_downs,
                            range_list_t*   p_ranges )
    {
        point_cnt_t     i, j;        
        float*          Up_region_plus  = NULL;
        float*          Down_region     = NULL;
        float           ReachEnd_val;
        float           ReachStart_val;
        range_t         D;
        point_cnt_t     SoC;
        float           SoC_val;
        point_cnt_t     EoC;
        float           after_EoC_val;
        float           min_val;


//        printStr("\t -> malloc Look_For_Clusters");
        Up_region_plus = (float*) malloc( (U.stop + 1 - U.start) * sizeof(float) );
        memcpy( Up_region_plus, 
                reach_dists + U.start, 
                (U.stop + 1 - U.start) * sizeof(float) );            

        for ( i = 0; i + N < U.stop + 1; i++ )
            Up_region_plus[ N - U.start + i ] = INFINITY;

        ReachEnd_val = Up_region_plus[ U.stop - U.start ];
        
        for ( i = p_steep_downs->len; i > 0; i-- )
        {
            D = p_steep_downs->list[ i-1 ];

            if ( Down_region )
            {
                free( Down_region );
                Down_region = NULL;
//                printStr("\t -> free Look_For_Clusters");
            }

//            printStr("\t -> malloc Look_For_Clusters");
            Down_region = (float*) malloc( (D.stop + 1 - D.start) * sizeof(float) );
            memcpy( Down_region, 
                    reach_dists + D.start, 
                    (D.stop + 1 - D.start) * sizeof(float) );

            ReachStart_val = Down_region[ 0 ];

            //4
            if ( ReachStart_val * (1 - XI) <= ReachEnd_val )
            {
                SoC = D.start;

                for ( j = 0; j < U.stop + 1 - U.start; j++ )
                    if ( Up_region_plus[ j ] >= ReachStart_val * (1 - XI) )
                        break;

                EoC = U.start + j;
            }
            else
            {
                for ( j = D.stop + 1 - D.start; j > 0 ; j-- )
                    if ( Down_region[ j-1 ] * (1 - XI) > reach_dists[ U.stop - 1 ] )
                        break;

                SoC = D.start + j - 1;

                EoC = U.stop;
            }

            //3a
            if ( EoC - SoC < MIN_CLUSTER_PTS )
                continue;

            //3b
            SoC_val = reach_dists[SoC];
            after_EoC_val = EoC < N ? reach_dists[EoC] : INFINITY;
            min_val = MIN( SoC_val * (1 - XI), after_EoC_val );    

            for ( j = SoC+1; j < EoC; j++ )
                if ( reach_dists[ j ] >= min_val )
                    break;

            if ( j < EoC )
                continue;

            // Yaaaaay, found a cluster
            // But only pick clusters that have the new point in them
            for ( j = SoC; j < EoC; j++ )
                if ( data_idxs[ j ] == N - 1 )
                {
//                    Add_To_Ordered_Ranges( p_ranges, SoC, EoC );
                    Append_Ranges( p_ranges, SoC, EoC );
                    break;
                }
        }

        if ( Down_region )
        {
            free( Down_region );
//            printStr("\t -> free Look_For_Clusters");
        }

        if ( Up_region_plus )
        {
            free( Up_region_plus );
//            printStr("\t -> free Look_For_Clusters");
        }
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    int8_t sign( int8_t x )
    {
        if ( x < 0 )
            return -1;

        if ( x == 0 )
            return 0;

        return 1;
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    int8_t Get_Trend( float     a, 
                        float   b)
    {
        if (a == b)
            return 0;

        if (a <= b * (1 - XI))
            return 2;

        if (a < b)
            return 1;

        if (a * (1 - XI) >= b)
            return -2;

        if (a > b)
            return -1;

        return 0;
    }



    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    void  Get_Clusters_Steepness( range_list_t*    p_ranges)
    {
        int8_t          trend;
        point_cnt_t     i;
        point_cnt_t     start;
        point_cnt_t     stop;
        point_cnt_t     tolerance_point_cnt;
        range_t         U;
        range_list_t    steep_downs = {.len=0, .list=NULL};
        bool            normal_state;
        int8_t          direction;


//        printStr("Get_Clusters_Steepness");

        // State machine processing reach distance vector
        // We are looking for steep down or up areas
        normal_state    = TRUE; // If TRUE, indicates that we are not in a steep region
        direction       = 0; // If we enter a steep region, we store its direction (up vs down)


        i = 0;
        while ( (i < N) || !normal_state )
        {
            trend = Get_Trend(i < N    ? reach_dists[i]   : INFINITY, 
                              i+1 < N  ? reach_dists[i+1] : INFINITY);

            if ( normal_state )
            {
                // Very steep region found, this could be the beginning or the end of a cluster
                if ( trend == -2 || trend == 2 )
                {
                    normal_state        = FALSE;

                    start               = i;
                    tolerance_point_cnt = 0;
                    direction           = sign( trend );
                }
            }
            else // We are in a steep region
            {
                // New sample is moving as indicated by trend, but is that in 
                // accordance with the original directions of this region?
                if ( direction * trend == 2 ) // Yes, it is
                    tolerance_point_cnt  = 0;
                else if ( direction * trend == -1 ) // Ooops, new sample moves in the opposite direction
                {
                    normal_state        = TRUE;

                    stop                = i - tolerance_point_cnt;

                    // If this was a steep down store it for later
                    if ( direction < 0 )
                        Append_Ranges( &steep_downs, start, stop );
                    else // Else, if this was a steep up, then see if this up forms a cluster with any of the downs
                    {
                        U.start = start;
                        U.stop  = stop;

                        Look_For_Clusters( U, 
                                            &steep_downs,
                                            p_ranges );
                    }
                }
                else if ( direction * trend == -2 ) // Ooops, new sample moves in the opposite direction
                {
                    // So much so that it is actually start of a new steep region

                    stop                = i - tolerance_point_cnt;

                    // If this was a steep down store it for later
                    if ( direction < 0 )
                        Append_Ranges( &steep_downs, start, stop );
                    else // Else, if this was a steep up, then see if this up forms a cluster with any of the downs
                    {
                        U.start = start;
                        U.stop  = stop;

                        Look_For_Clusters( U, 
                                            &steep_downs,
                                            p_ranges);
                    }

                    start               = i;
                    tolerance_point_cnt = 0;
                    direction           = sign( trend );
                }
                else // Hmmm... Not going up, not going down, we tolerate some of these points in the region
                {
                    tolerance_point_cnt += 1;

                    // OK, so, we had too many neutral points, that's not a single region anymore
                    if ( tolerance_point_cnt >= STEEP_TOLERANCE_PTS )
                    {
                        normal_state        = TRUE;

                        stop                = i - tolerance_point_cnt + 1;

                        // If this was a steep down store it for later
                        if ( direction < 0 )
                            Append_Ranges( &steep_downs, start, stop );
                        else // Else, if this was a steep up, then see if this up forms a cluster with any of the downs
                        {
                            U.start = start;
                            U.stop  = stop;

                            Look_For_Clusters( U, 
                                                &steep_downs,
                                                p_ranges );
                        }
                    }
                }
            }

            i += 1;
        }

        if ( steep_downs.list )
        {
            free( steep_downs.list );
//            printStr("\t -> free Get_Clusters_Steepness");
        }
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    task void Classify()
    {
        point_cnt_t             i;
        point_cnt_t             d;
        point_cnt_t             j;

        range_list_t            ranges = {.len=0, .list=NULL};
        classification_msg_t    classification_msg;

//        printStr("Classify");

        // Shift FIFO vector containing measurement points
        memmove( data, &data[1], (N-1) * DATA_DIMENSION * sizeof(data_t) );
        // Copy new data point to last place
        memmove( &data[N-1], &new_data, DATA_DIMENSION * sizeof(data_t) );

        Generate_Reach_Dist();

//        printStr("Reach dists");
//        for ( i = 0; i < N; i++ )
//            printInt32( (int32_t) reach_dists[i] );

        // Get all the clusters that formed within the measurement space
        Get_Clusters_Steepness( &ranges );

        qsort(ranges.list, 
            ranges.len, 
            sizeof(range_t), 
            Compare_Ranges );         

        // Take only the first cluster -> i=0 and only 0
//        for ( i = 0; i < ranges.len; i++ )
        for ( i = 0; i < 1; i++ )
        {
//            printStr("ranges");
//            printInt32( (int32_t) i);

//            printStr("size");
//            printInt32( (int32_t) ranges.list[i].stop - ranges.list[i].start );

            // Create message from cluster size
            classification_msg.cluster_size = ranges.list[i].stop - ranges.list[i].start;

//            printStr("cluster_size");
//            printInt32( (int32_t) classification_msg.cluster_size );

            if ( classification_msg.cluster_size == 0)
                break;

            // Calculate cluster mean and varianve
            for ( d = 0; d < DATA_DIMENSION; d++ )
            {
                float acc;

                printStr("dimension");
                printInt32( (int32_t) d );

                //Mean
                acc = 0;
                for ( j = ranges.list[i].start; j < ranges.list[i].stop; j++ )
                    acc += (float) data[ data_idxs[j] ][d];

                classification_msg.mean[d] = acc / classification_msg.cluster_size; 

                printStr("mean");
                printInt32( (int32_t) (classification_msg.mean[d]) );


                //Variance
                acc = 0;
                for ( j = ranges.list[i].start; j < ranges.list[i].stop; j++ )
                    acc += pow( (float) data[ data_idxs[j] ][d] - classification_msg.mean[d], 2);

                classification_msg.variance[d] = acc / (classification_msg.cluster_size - 1); 

                printStr("variance");
                printInt32( (int32_t) (classification_msg.variance[d]) );
            }
            
        }

        if ( ranges.list )
        {
            free( ranges.list );
//            printStr("\t -> free Classify");
        }

        // Send cluster result to main core
        if ( classification_msg.cluster_size )
            signal output.readDone( SUCCESS, classification_msg );

    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    command error_t output.read()
    {
        return SUCCESS;
    }


    ////////////////////////////////////////////////
    //
    //
    //
    ////////////////////////////////////////////////
    event void input.readDone( error_t result, Event_t val )
    {
        uint8_t i;

//        printStr("Optics_ClusteringP input.readDone");

        if (result != SUCCESS)
            return;

        // Adding new data point to measurement space
        // Find the lowest log quality index among all channels -> first dimension
        new_data[0] = DATA_MAX;
        for ( i = 0; i < NUMBER_OF_EVENT_CHANNELS; i++ )
            if ( new_data[0] > val.log_quality_idx_m[i] )
                new_data[0] = val.log_quality_idx_m[i];

        // Copy time difference data -> fill up all other dimensions
        for ( i = 0; i < NUMBER_OF_EVENT_CHANNELS-1; i++ )
            new_data[i+1] = val.time_diff_us[i]; 

        post Classify();
    }
}
