#include "Events_Descriptor.h"
#include "Classification_Descriptor.h"
#include "AvroraPrint.h"

module Event_ClassifierP 
{
    uses       interface Read<Event_t>               as input;

	provides   interface Read<classification_msg_t>  as output;

    uses       interface Read<classification_msg_t>  as from_clustering;

    provides   interface Read<Event_t>               as to_clustering;
}
implementation
{
    //Event_t        event;

    command error_t output.read()
    {
        //printStr("output.read()");

        return SUCCESS;
    }

    command error_t to_clustering.read()
    {
        //printStr("to_clustering.read()");

        return SUCCESS;
    }

    event void input.readDone( error_t result, Event_t val )
    {
//        printStr("Event arrived");

        if (result != SUCCESS)
            return;

        signal to_clustering.readDone( SUCCESS, val );
    }

    event void from_clustering.readDone( error_t result, classification_msg_t val )
    {
        printStr("Event classified");

        if (result != SUCCESS)
            return;

        signal output.readDone( SUCCESS, val );
    }
}
