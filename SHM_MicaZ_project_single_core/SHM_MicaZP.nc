#include "RadioMsg.h"
#include "Classification_Descriptor.h"

#include "AvroraPrint.h"


#define TIMER_PERIOD_MILLI  1000
#define NODE_ID             1


module SHM_MicaZP
{
    uses interface Boot;
    
    uses interface Read<classification_msg_t> as Read;

	uses interface RFMsgSend;

    //Radio TX
    //AMSenderC

	//ActiveMessageC
    uses interface SplitControl as AMControl;
    
    //Radio RX
    uses interface Receive;
}
implementation
{
	uint16_t temp_counter = 0;

    event void Boot.booted()
    {
// 	    printStr("Starting.");

		//This is not fully split-phase
		//The return value is supposed indicate whether the radio interface could be started or not
		//but the wrapper will always report success
        call AMControl.start();

//   		printStr("Started.");
//		printInt8(source_ID);
    }

    event void AMControl.startDone(error_t err)
    {
        if (err != SUCCESS)
		{
			//This is not fully split-phase
			//The return value is supposed indicate whether the radio interface could be started or not
			//but the wrapper will always report success

    		printStr("AMControl.startDone: err != SUCCESS.");

            call AMControl.start();
            return;
		}

//    	printStr("AMControl.startDone.");
    }

    event void AMControl.stopDone(error_t err) 
    {
    }

    event void Read.readDone( error_t result, classification_msg_t val )
    {
// 	    printStr("Read done.");

		if (result != SUCCESS)
			return;

//        printStr("Read done.");

		/*
		//This is not fully split-phase
		//The return value is supposed indicate whether the processing could be started or not
		//but the wrapper will always report success
		call SignalProcess.proc(val);
		*/
		//HACK - no signal processing for now, instead
		call RFMsgSend.SendRadioMsg( NODE_ID, &val, sizeof(classification_msg_t) );
    }
    
    event message_t* Receive.receive(message_t*	msg, 
									void*		payload, 
									uint8_t		len) 
    {
//		RadioMsg* rpkt;

        if ( len != sizeof(RadioMsg) )
            return msg; //Not doing anything with this after return
    
//        rpkt = (RadioMsg*) payload;
//		rpkt->nodeid 
//		rpkt->val

        call RFMsgSend.SendRadioMsg( NODE_ID, payload, len );

        return msg; //Not doing anything with this after return
    }

}