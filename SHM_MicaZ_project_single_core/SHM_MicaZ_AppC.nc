#include <Timer.h>

enum 
{
	AM_RADIO	= 6
};

configuration SHM_MicaZ_AppC
{
}
implementation
{
	/////////////////////////////////
	// Components

	components MainC;
	components SHM_MicaZP;

	components Event_ClassifierP;
	components new Optics_ClusteringP()	as OCP;

	components new TimerMilliC()	as Timer;
	components new AE_analyserP()	as AE_analysisP;

	components ActiveMessageC;
	components new AMSenderC(AM_RADIO);
	components new AMReceiverC(AM_RADIO);
	components AMSenderIFP;


	/////////////////////////////////
	// Connections
	AE_analysisP.Timer					-> Timer;
	AE_analysisP.Boot					-> MainC;

	Event_ClassifierP.input				-> AE_analysisP.output;
	OCP.input							-> Event_ClassifierP.to_clustering;
	Event_ClassifierP.from_clustering	-> OCP.output;

	SHM_MicaZP.Boot						-> MainC;
	SHM_MicaZP.Read						-> Event_ClassifierP.output;

	//Radio TX
	AMSenderIFP.Packet					-> AMSenderC;
	AMSenderIFP.AMSend					-> AMSenderC;
	SHM_MicaZP.RFMsgSend				-> AMSenderIFP;

	SHM_MicaZP.AMControl				-> ActiveMessageC;

	//Radio RX
	SHM_MicaZP.Receive					-> AMReceiverC;
}
